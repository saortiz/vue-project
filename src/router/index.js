import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import UserLogin from '../components/users/UserLogin.vue'
import UserRegister from '../components/users/UserRegister.vue'
import PersonView from '../views/PersonaView.vue'
import FilmView from '../views/FilmView.vue'
import LogDataView from '../views/LogDataView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      // Guard para evitar el ingreso nuevamente
      // beforeEnter:(to, from, next) => {
      //   if(localStorage.getItem('token') === null) next({name: 'person'})
      //   else next()
      // },
      children: [
        {
          name:'login',
          path: '/login',
          component: UserLogin
        },
        {
          path: '/register',
          component: UserRegister
        }
      ]
    },
    {
      path: '/person',
      name: 'person',
      component: PersonView,
      // Guard para evitar el ingreso sin autenticación
      beforeEnter:(to, from, next) => {
        console.log(localStorage.getItem('token'));
        if(localStorage.getItem('token') === null) next({name: 'login'})
      }
    },
    {
      path: '/film',
      name: 'film',
      component: FilmView
    },
    {
      path: '/logData',
      name: 'logData',
      component: LogDataView
    }
  ]
})



export default router
